THIS PROGRAM IS OUT OF DATE, AND WILL NOT BE UPDATED!













This program is a client manager for PoE. It is writen to ExiledBot for 24/7 botting.
Note: Program uses ImageSearch, so may not work with bright bug on VM, but works with the flickering.

Features:
  - Communication with ExiledBot
  - Can handle username and password (if needed to retype)
  - Can handle e-mail vertification (non SSL e-mail server needed, like mail.aol.com, SSL servers will not work)
  - Can detect client loading idle
  - Can detect bot idle
  - Can shutdown computer on PoE update
  - Fully configurable (For now only with text editor (ini file))
  - F10: Panic (will instantly close the program (not the bot nor the client))

How to install:
  - This program does not have to be installed, just download the autopoe.exe and the Resources directory
    and place them somewhere together and you are done.
  
How to setup:
  Run autopoe.exe once, it will create the settings file. After that it will asks if you want to open it,
  click on the "Yes" button in the messagebox. It will open the config with your default ini editor.
  Help will be at the top of the file, setup everything as you want then run autopoe.exe again. Happy botting.

Help for config (also found in config file):
  poe section:
    user               -> Your account name (e-mail)
    pass               -> Your account password
    folderpath         -> The directory where the "PathOfExile.exe" can be found
    arguments          -> Command line arguments for PathOfExile.exe (Read more: http://www.pathofexile.com/forum/view-thread/321900)
    timeout            -> Restart if idle for this amount of time in minutes (If not logged in, set higher then 10 with slow computer)
    shutdown_on_update -> Shutdown computer on PoE update (for safety reason)  (0 = Disabled, 1 = Enabled)
    max_update_time    -> Restart if update is not finished after this amount of time in minutes. (0 means disabled)
    max_fail_login     -> How many times try to login (0 means disabled)
    on_max_fail_login  -> 0: Disabled, >=1: Wait X seconds, -1: Close program, <=-2: Shutdown computer
                          Example use:
                            This will close the program if failed to login 3 times in a row:
                              max_fail_login=3
                              on_max_fail_login=-1
                            
                            This will shutdown the computer after 5 login error in a row:
                              max_fail_login=5
                              on_max_fail_login=-2
                            
                            This will wait 10 seconds before re-trying to login (after 2 login error):
                              max_fail_login=2
                              on_max_fail_login=10

  bot section:
    folderpath         -> The directory where the "ExiledBot.exe" can be found
    timeout            -> Restart if idle for this amount of time in minutes. (If logged in running with bot)

  mail section:
    user               -> Your e-mail address (Empty means disabled)
    pass               -> Your e-mail password (Empty means disabled)
    pop3               -> Your e-mail host's pop3 server (SSL does not working, mail.aol.com is recommended)
    port               -> Your e-mail host's server port (usually 110)
    wait_time          -> Wait this amount of time in seconds before retrying e-mail reading

  settings section
    logging            -> Enable logging (0: Disabled, 1: Enabled)
    debugging          -> Enable debug logging (a lots of log) (0: Disabled, 1: Enabled)
    log_file           -> Log file (if enabled)
    click_delay        -> Program will not click again for this amount of time in seconds

  scripts section
    runfolder_         -> Will run every file with the extension specified down (NOTE: write key after _, eg a number)
    runext_            -> Will set the extensions (NOTE: write key after _, this will link the two settings together)
    runscript          -> If you don't want to run every script in a folder you can use this, it will run only one file

       Example usage:
          runfolder_1=C:\Scripts
          runext_1=exe|ahk
          ^ This two line will run every ahk and exe file found in C:\Scripts ^

          runfolder_2=C:\Scripts_2
          runext_2=exe
          ^ This two line will run every exe file found in C:\Scripts_2 ^          

          runscript=C:\OneScriptOnly\test.ahk
          ^ This line will runy only test.ahk found in C:\OneScriptOnly ^

    NOTE: It will not detect if script is running or not, also it will close process (if exe extension) before run,
          and AutoHotKey.exe before running autohotkey scripts

Communication with ExiledBot:
  With the newest version of the bot it will can communicate with the bot, so it will have much more accurate bot crash detection,
  it will detect crash in 10 seconds. No more unneeded bot closing, it will only close the bot if it is crashed or timed out.
  Also with the new system if you pause the bot it will pause the script too, so if you want to manage your character it will not
  close the client/bot because of the timeout.

  (Note: For best perfomance change the exit_to_login_button_y in the coordinates.ini to 260 so it will look like this:
   exit_to_login_button_y=260
   with this change the bot will click on the "Exit to character selection" button instead of the "Exit to login screen" button)

Tested on:
  - VMWare Workstation 10.0.1 build-1379776 with Windows 7
  - Normal computer with Windows 8

Download:  https://bitbucket.org/BlackSun7/autopoe
Bugreport: http://exiled-bot.net/community/index.php/topic/697-auto-poe/
Donate:    https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=7PS9XFX75JY6G

If you like my program feel free to donate.

Changelog:
  v1.16:
    - Offsets updated for new PoE version (v1.1.4b)

  v1.15:
    - Offsets updated for new PoE version (v1.1.3)

  v1.14b:
    - Fixed a bug with "on_max_fail_login" and "max_fail_login" where the program used to read "max_fail_login" for delay (instead of "on_max_fail_login")

  v1.14a:
    - Fixed the two new settings, added more specific informations, read above
    - Fixed wrong version numbers

  v1.14:
    - Added two settings under the [poe] section: "max_fail_login" and "on_max_fail_login", read above to check how to use them
    - The program now will not spam the same e-mail vertification code

  v1.13:
    - Offsets updated for new PoE version (v1.1.2)

  v1.12a:
    - Offsets updated for new PoE version (1.1.1c)
    - Now the script will try to delete the e-mail after it is already used

  v1.12:
    - The script now will close the ExiledBot crash window
    - At startup the script now will check the Resource files (ImageSearchDLL.dll, and the images) and inform the user
	  if something is wrong with it (NOTE: It is only check the file name specified in the autopoe_config.au3 source)
    - The script now will release keyboards at PoE startup and bot startup etc. Sometimes alt get stucked for me and
	  the bot cannot open npc/stash because of it (NOTE: I don't know if it will fix it)

  v1.11a:
    - Offsets updated for new PoE version (1.1.1b)

  v1.11:
    - Updated for new PoE version (1.1.1), offsets are working, resource files updated

  v1.10:
    - Updated for new PoE version (1.1.0d), but it maybe need a Resource update (for buttons etc)

  v1.9b:
    - (Maybe) fixed the AutoPoE crash
	- Error window detection changed
	- Abnormal disconnection message is now detected (didn't test it)

  v1.9a:
    - Garena version "support" removed, I changed my mind, I'll not support garena version
    - Updated for new PoE version (v1.0.6b)

  v1.9:
    - Error window detection reworked, (maybe) now it will close all error windows
    - Added garena version, for more information read the "Help for config" section above

  v1.8e:
    - (Maybe) now it will work with PoE v1.0.6

  v1.8d:
    - Offsets updated to PoE v1.0.6

  v1.8c:
    - Fixed a bug with The Catacombs

  v1.8b:
    - Fixed a bug where the script stucked at the first notify window (for non-elite users)

  v1.8a:
    - Maybe fixed the "ExiledBot has stopped working" window bug, where the script didn't close it

  v1.8:
    - Source code split into smaller files
	- Status detecting reworked: now it will not spam the game with unneeded MemoryRead also less spammy with the bot too
	- Offsets updated for new PoE version
	- Smart bot executable detection, now it will find it if there is "ExiledBot" in it's name and its an .exe file

  v1.7b:
    - Added delay to bot controller so it will not spam the bot (maybe crash fix?)
	- From now the script will use OpenProcess in PROCESS_VM_READ access right (maybe crash fix?)

  v1.7a:
    - Fixed a bug where on character selection the script opened and closed the bot
	- Now the script will detect "Application Error" window and will close the bot immediately

  v1.7 fix:
    - Fixed a bug where the script detected bot as crashed, while it was not.

  v1.7:
    - Offsets updated for new PoE version
    - Script will now communicate with ExiledBot (see above)
    - It will now work with the "ExiledBot GUI.exe" bot process name too

  v1.6a:
    - Offset updated for new PoE version

  v1.6:
    - Program will no longer close bot on loading screen
    - Added new setting: arguments (Read more above)
    - Removed unneeded bitmaps
    - removed life check

  v1.5c:
    - Hotkey buttons can now be changed in config (see more about it above at config help)

  v1.5b:
    - Fixed the previous bug again, added life reading from poe to detect if logged in or not
    - Pause function added to F9 (press F9 to pause the script and F9 again to start it)
      (NOTE: It will pause at the loop begining so it may do some other step after you paused,
       before actually stopping)

  v1.5a:
    - Fixed a bug where the program opened and then closed the bot instantly

  v1.5:
    - Program now read memory to detect login status, will only search for image (to login etc) if not logged in

  v1.4b:
    - Program sometimes didn't click on the start button (in bot)

  v1.4a:
    - Script management updated, now it will start scripts when needed (with bot) and close them
      when they are not needed

  v1.4:
    - Auto script launching added (alpha, it could be buggy)
      (NOTE: For now it will only start scripts when the program starts)

  v1.3:
    - ImageSearch tolerance increased from 10 to 50, so it will work for new PoE

  v1.2a:
    - Small bugfix (a typoo)

  v1.2:
    - Switched to ImageSearch for better stability
    - Added Readme.txt

  v1.1a:
    - bugfix

  v1.1:
    - Much better status detection

  v1.0:
    - Initial release