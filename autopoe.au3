#region Options
	#RequireAdmin				; Required to handle the bot
	Opt("MouseCoordMode", 2)	; Set click coord mode to relative to the current window's client
#endregion Options

#region Includes
	#include "pop3.au3"     	; Required for e-mail vertification handling
	#include "hash.au3"			; Required for key, value pairs (easier settings)
	#include "ImageSearch.au3"	; Required for status detecting
	#include "NomadMemory.au3"	; Required for Memory Reading (status detection)
	
	#include "autopoe_config.au3"
	#include "autopoe_hotkeys.au3"
	#include "autopoe_other.au3"
	#include "autopoe_winhandler.au3"
	#include "autopoe_winmsg.au3"
	#include "autopoe_mailreader.au3"
#endregion Includes

#region Global variables
	; Timers
	Global $tmr_poe_timeout = -1		; For timeout detection (see in poe section help)
	Global $tmr_bot_timeout = -1		; For timeout detection (see in bot section help)
	Global $tmr_update = -1				; For max update time
	Global $tmr_last_loop = -1			; To prevent spamming the main loop
	
	; Other
	Global $last_exp = 0				; Required for idle detection (while logged in)
	Global $last_email_vertif = ""		; Required for preventing spamming the same and wrong code
	Global $failed_login = 0			; Required for preventing the login-spam
	
	; Memory addresses
	Global $mem					= 0			 								; Used for memory reading
	
	; Setup different offsets for different PoE version
	Global $login_address		= 0x007FC440								; Address of the loggedin status (character OR character selection screen)
	Global $login_base			= HashLookup($poe, "exe")					; Base for loggedin status
	Global $cur_exp_address		= 0x007FC22C								; Required for idle detection (while logged in)
	Global $cur_exp_base		= HashLookup($poe, "exe")					; Base for current experience
	Global $cur_exp_offset[8]	= [0x4,0x7C,0x9C,0xBC,0x5A0,0x4,0x14,0x34]	; Required for idle detection
#endregion Global variables

#region Functions
	; Release all bugged modifier key (ALT, SHIFT etc...)
	Func ReleaseKeys()
		Local $buttons[7] = ["ALT", "LALT", "RALT", "LCTRL", "RCTRL", "LSHIFT", "RSHIFT"]
		
		For $i = 0 To UBound($buttons) - 1
			Send("{" & $buttons[$i] & " UP}")
		Next
		
		Dim $buttons[3] = ["ALTUP", "CTRLUP", "SHIFTUP"]
		
		For $i = 0 To UBound($buttons) - 1
			Send("{" & $buttons[$i] & "}")
		Next
	EndFunc
	
	; Sets the status of the bot
	Func SetBotStatus($WM)
		If ProcessExists(HashLookup($bot, "exe")) Then
			Local $bot_title = ProcessGetWin(HashLookup($bot, "exe"))
			
			If HashLookup($bot, "handle") <> WinGetHandle($bot_title) Then
				AddDebug("Bot handle updated to " & WinGetHandle($bot_title) & " from " & HashLookup($bot, "handle"))
				HashUpdate($bot, "handle", WinGetHandle($bot_title))
			EndIf
			
			AddDebug("Setting bot status to 0x" & Hex($WM, 4) & " in handle: " & HashLookup($bot, "handle"))
			
			If PostWM(HashLookup($bot, "handle"), $WM) Then
				AddDebug("Command sent to the bot")
				
				$msg = WaitWM()
				If $msg Then
					AddDebug("Bot answered: 0x" & Hex($msg, 4))
					Return True
				Else
					AddDebug("Bot didn't answer")
				EndIf
			Else
				AddDebug("Couldn't send command to the bot")
			EndIf
		Else
			AddDebug("Bot is not running")
		EndIf
		
		Return False
	EndFunc
#endregion Functions

#region Status checker
	; Returns the current status of the bot
	Func GetBotStatus()
		If ProcessExists(HashLookup($bot, "exe")) Then
			Local $bot_title = ProcessGetWin(HashLookup($bot, "exe"))
			
			If HashLookup($bot, "handle") <> WinGetHandle($bot_title) Then
				AddDebug("Bot handle updated to " & WinGetHandle($bot_title) & " from " & HashLookup($bot, "handle"))
				HashUpdate($bot, "handle", WinGetHandle($bot_title))
			EndIf
			
			; Check for error windows
			Local $bot_titles = ProcessGetWins(HashLookup($bot, "exe"))
			
			If StringLen($bot_titles > 0) And StringInStr($bot_titles, ";") <= 0 Then
				If Compare($bot_titles, HashLookup($bot, "error")) Then
					AddDebug("Bot status return: crash")
					Return "crash"
				EndIf
			Else
				Local $parts = StringSplit($bot_titles, ";")
				
				For $i = 1 To $parts[0]
					If Compare($parts[$i], HashLookup($bot, "error")) Or Compare($parts[$i], HashLookup($bot, "error2")) Or Compare($parts[$i], ".*[iI]nformation.*") Then
						AddDebug("Bot status return: crash")
						Return "crash"
					EndIf
				Next
			EndIf
			
			If Compare($bot_title, HashLookup($bot, "notify")) Then
				Return "notify"
			EndIf
			
			If PostWM(HashLookup($bot, "handle"), $WM_STATUS) Then
				Switch WaitWM()
					Case $WM_RUNNING
						AddDebug("Bot status return: running")
						Return "running"
					Case $WM_STOPPED
						AddDebug("Bot status return: stopped")
						Return "stopped"
					Case $WM_PAUSED
						AddDebug("Bot status return: paused")
						Return "paused"
					Case 0
						AddDebug("Bot crash: no answer")
						Return "crash"
				EndSwitch
			Else
				AddDebug("Bot crash: couldn't send WM")
				Return "crash"
			EndIf
		Else
			AddDebug("Bot status return: no_bot")
			Return "no_bot"
		EndIf
	EndFunc
	
	; Returns the current status
	Func GetStatus()
		If ProcessExists(HashLookup($poe, "exe")) then
			Local $poe_title = ProcessGetWin(HashLookup($poe, "exe"))
			
			; Update window handle, process ID and open process for MemoryReading
			If HashLookup($poe, "handle") <> WinGetHandle($poe_title) Then
				AddDebug("PoE handle updated to " & WinGetHandle($poe_title) & " from " & HashLookup($poe, "handle"))
				HashUpdate($poe, "handle", WinGetHandle($poe_title))
			EndIf
			
			If Not IsArray($mem) Or HashLookup($poe, "pid") <> WinGetProcess($poe_title) Then
				AddDebug("PoE pid updated to " & WinGetProcess($poe_title) & " from " & HashLookup($poe, "pid"))
				HashUpdate($poe, "pid", WinGetProcess($poe_title))
				$mem = _MemoryOpen(HashLookup($poe, "pid"))
			EndIf
			
			; Check if the error window is up in the launcher
			If _WinExists(HashLookup($poe, "error")) Then
				AddDebug("Status return: error")
				Return "error"
			EndIf
			
			; Check for other error windows
			Local $poe_titles = ProcessGetWins(HashLookup($poe, "exe"))
			
			If StringLen($poe_titles > 0) And StringInStr($poe_titles, ";") <= 0 Then
				If Compare($poe_titles, HashLookup($poe, "error2")) Then
					AddDebug("Status return: error")
					Return "error"
				EndIf
			Else
				Local $parts = StringSplit($poe_titles, ";")
				
				For $i = 1 To $parts[0]
					If Compare($parts[$i], HashLookup($poe, "error2")) Then
						AddDebug("Status return: error")
						Return "error"
					EndIf
				Next
			EndIf
			
			; Check if launcher is updating
			If  Compare($poe_title, HashLookup($poe, "updating")) Or _
				Compare($poe_title, HashLookup($poe, "checking_resources")) Or _
				Compare($poe_title, HashLookup($poe, "allocating_space")) Then

				If Compare(ControlGetText($poe_title, "", "[CLASS:RichEdit20W; INSTANCE:1]"), ".*Path of Exile - Patch Notes.*") Then
					AddDebug("Status return: updating")
					Return "updating"
				Else
					AddDebug("Status return: checking_resources")
					Return "checking_resources" ; Launcher is loading, do nothing
				EndIf
			EndIf
			
			; Check if update is completed
			If Compare($poe_title, HashLookup($poe, "updated")) Then
				AddDebug("Status return: updated")
				Return "updated"
			EndIf
			
			; Check login status
			
			Local $loggedin = _MemoryRead(_MemoryGetBaseAddress($mem, $login_base) + $login_address, $mem, "byte")
			Local $cur_exp = _MemoryReadPointer(_MemoryGetBaseAddress($mem, $cur_exp_base) + $cur_exp_address, $cur_exp_offset, $mem)
			
			If $loggedin <= 0 And $cur_exp <= 0 Then
				If Search("login", $poe) Then
					AddDebug("Status return: login")
					Return "login"
				EndIf
				
				If Search("loginerror", $poe) Then
					AddDebug("Status return: login_error")
					Return "login_error"
				EndIf
				
				If Search("abnormaldisconnection", $poe) Then
					AddDebug("Status return: login_error (Note: Abnormal Disconnection)")
					Return "login_error"
				EndIf
				
				If Search("unlock", $poe) Then
					AddDebug("Status return: mail")
					Return "mail"
				EndIf
				
				If Search("unlocked", $poe) Then
					AddDebug("Status return: unlocked")
					Return "unlocked"
				EndIf
			ElseIf $loggedin > 0 Or $cur_exp > 0 Then
				AddDebug("Status return: loggedin")
				Return "loggedin"
			EndIf
			
			AddDebug("Status return: loading")
			Return "loading" ; PoE process exists but none of the above is true, so it's still loading
		EndIf
		
		; PoE not running
		AddDebug("Status return: no_poe")
		Return "no_poe"
	EndFunc
#endregion Status checker

#region Initialization
	; Adding initial log to know the program startup time
	AddLog("System startup")
	TraySetToolTip("Auto PoE Restarter")
	
	If Not IsAdmin() Then
		MsgBox(0, "Error", "Please run Auto PoE as administrator")
		Exit
	EndIf
	
	; Check resources
	If Not FileExists(@ScriptDir & "\Resources") Then
		MsgBox(0, "Error", "Resource directory not found.")
	EndIf
	
	If	Not CheckResource($controls, "login") Or _
		Not CheckResource($controls, "unlock") Or _
		Not CheckResource($controls, "loginerror") Or _
		Not CheckResource($controls, "abnormaldisconnection") Or _
		Not CheckResource($controls, "unlocked") Or _
		Not FileExists(@ScriptDir & "\Resources\ImageSearchDLL.dll") Then
		
		MsgBox(0, "Error", "One or more resource file is missing.")
		Exit
	EndIf
	
	; Exit if PoE or bot is not exists
	If Not FileExists(HashLookup($config_poe, "folderpath") & "\" & HashLookup($poe, "exe")) Then
		AddLog("PathOfExile.exe not found")
		MsgBox(0, "Error", "PathOfExile.exe not found")
		Exit
	EndIf
	
	If Not FileExists(HashLookup($config_bot, "folderpath") & "\" & HashLookup($bot, "exe")) Then
		AddLog("ExiledBot not found")
		MsgBox(0, "Error", "ExiledBot not found")
		Exit
	EndIf
	
	; Get all script from the setting
	GetScripts()
#endregion Initialization

#region Loop
	While 1
		; To prevent loop spamming sleep until 1 sec is passed after the last loop
		If Not (TimerDiff($tmr_last_loop) >= 1000 Or $tmr_last_loop = -1) Then
			AddDebug("Sleep required before loop")
			Sleep(1000 - TimerDiff($tmr_last_loop)) ; Sleep as much as need for the 1 sec delay
		EndIf
		$tmr_last_loop = TimerInit() ; Initialize timer
		
		$bot_status = GetBotStatus()
		
		; Pause the script if player paused or bot is paused
		If $paused Or $bot_status = "paused" Then
			; Clear timeouts to prevent PoE and bot closing
			$tmr_poe_timeout = -1
			$tmr_bot_timeout = -1
			TrayStatus("paused", $bot_status)
			ContinueLoop
		EndIf
		
		; Get current status
		$status = GetStatus()
		
		; Reset timeout counters if they are not needed
		If $status <> "loading" And $bot_status <> "running" Then
			AddDebug("Loading timeout cleared")
			$tmr_poe_timeout = -1
		EndIf
		
		If Not ($status = "loggedin" And $bot_status = "running") Then
			AddDebug("Bot timeout cleared")
			$tmr_bot_timeout = -1
		EndIf
		
		; Update tooltip to the current status
		AddDebug("Current statuses: " & $status & ", " & $bot_status)
		TrayStatus($status, $bot_status)
		
		; Stop/Close bot and close scripts if no need for them (to prevent bugs)
		If ProcessExists(HashLookup($bot, "exe")) And $status <> "loggedin" And $status <> "loading" And $bot_status = "running" Then
			AddDebug("Closing scripts")
			ReleaseKeys()
			Close("script")
			
			If Not SetBotStatus($WM_STOPBOT) Then
				AddDebug("Bot didn't answer, closed")
				Close("bot")
			EndIf
		EndIf
		
		Switch $status
			Case "loading" ; PoE window is up but it's still loading, checking for timeout
				If $tmr_poe_timeout = -1 Then
					AddDebug("Loading timeout initialized")
					$tmr_poe_timeout = TimerInit()
					ContinueLoop ; Timer initialized, no need further things to do
				EndIf
				
				If TimerDiff($tmr_poe_timeout) >= HashLookup($config_poe, "timeout") * 60 * 1000 Then ; Timed out, restarting
					AddLog("Client timed out, restarting")
					$tmr_poe_timeout = -1
					ReleaseKeys()
					Close("poe&script")
					
					If Not SetBotStatus($WM_STOPBOT) Then
						AddDebug("Bot didn't answer, closed")
						Close("bot")
					EndIf
				EndIf
			Case "no_poe" ; Nothing runnig, starting PoE
				If MsgBox(262148, "Startup", "Do you want to start Path of Exile?" & @CRLF & "(NOTE: Auto yes after 5 seconds)", 5) = 7 Then Exit
				
				AddLog("Starting client")
				If StringLen(HashLookup($config_poe, "arguments")) > 0 Then
					ReleaseKeys()
					ShellExecute(HashLookup($config_poe, "folderpath") & "\" & HashLookup($poe, "exe"), HashLookup($config_poe, "arguments"), HashLookup($config_poe, "folderpath"), "open")
				Else
					ReleaseKeys()
					Run(HashLookup($config_poe, "folderpath") & "\" & HashLookup($poe, "exe"), HashLookup($config_poe, "folderpath"))
				EndIf
				
				; Wait until one of the PoE windows show up
				WaitWin(HashLookup($poe, "name") & ";" & HashLookup($poe, "error") & ";" & HashLookup($poe, "checking_resources") & ";" & HashLookup($poe, "allocating_space") & ";" & HashLookup($poe, "updating")& ";" & HashLookup($poe, "updated"))
			Case "loggedin" ; Logged in, checking bot status
				Switch $bot_status
					Case "crash"
						AddLog("Bot crashed")
						ReleaseKeys()
						Close("bot&script&error")
					Case "no_bot"
						AddLog("Running bot")
						
						ReleaseKeys()
						Run(HashLookup($config_bot, "folderpath") & "\" & HashLookup($bot, "exe"), HashLookup($config_bot, "folderpath"))
						WaitWin(HashLookup($bot, "name") & ";" & HashLookup($bot, "notify"))
						Sleep(5000) ; Wait 5 second to let the bot load
					Case "notify"
						WinClose(_WinGetTitle(HashLookup($bot, "notify")))
						WaitWin(HashLookup($bot, "name") & ";" & HashLookup($bot, "notify"))
					Case "stopped"
						Close("script") ; Close scripts before run them (restart)
						ReleaseKeys()
						RunScripts() ; Run every script
						
						AddLog("Starting bot")
						
						If Not SetBotStatus($WM_STARTBOT) Then
							AddLog("Bot didn't answer, restarting")
							ReleaseKeys()
							Close("script&bot")
						EndIf
					Case "running" ; Bot running, checking for idle
						If $tmr_bot_timeout = -1 Then
							AddDebug("Bot timeout initialized")
							$tmr_bot_timeout = TimerInit()
							$last_exp = 0
							ContinueLoop ; Timer initialized, no need further things to do
						EndIf
						
						; Check if bot is stucked or not (no monster killing)
						Local $cur_exp = _MemoryReadPointer(_MemoryGetBaseAddress($mem, $cur_exp_base) + $cur_exp_address, $cur_exp_offset, $mem)
						
						If $cur_exp = $last_exp  Then
							If TimerDiff($tmr_bot_timeout) >= HashLookup($config_bot, "timeout") * 60 * 1000 Then ; Timed out, restarting
								AddLog("Bot timed out, restarting")
								$tmr_bot_timeout = -1
								ReleaseKeys()
								Close()
							EndIf
						Else
							$tmr_bot_timeout = TimerInit()
							$last_exp = $cur_exp
						EndIf
				EndSwitch
			Case "error" ; Launcher failed to connect to the server, restart everything
				AddLog("Launcher failed to connect to the server, restarting")
				ReleaseKeys()
				Close() ; Close everything
			Case "updating" ; Launcher updating
				AddLog("Launcher updating")
				
				If HashLookup($config_poe, "shutdown_on_update") > 0 Then
					AddLog("Shutting down computer (due to settings, see: shutdown_on_update)")
					
					If MsgBox(262148, "Shutdown", "Game is updating, do you want to shutdown?" & @CRLF & "Click yes to shutdown now or no to close this program." & @CRLF & "(Note: Auto shutdown after 30 seconds)", 30) = 7 Then Exit
					
					Shutdown(5) ; Force shutdown the computer
					Exit
				EndIf
				
				If HashLookup($config_poe, "max_update_time") > 0 Then
					If $tmr_update = -1 Then
						$tmr_update = TimerInit() ; Reset update timer
						ContinueLoop ; Timer initialized no need further things to do here so continue loop
					EndIf
					
					If TimerDiff($tmr_update) >= HashLookup($config_poe, "max_update_time") * 60 * 1000 Then
						AddLog("Maximum update time exceeded, restarting")
						$tmr_update = -1 ; Unload update timer
						ReleaseKeys()
						Close() ; Close PoE and the bot
					EndIf
				EndIf
			Case "updated"
				AddLog("Updated, clicking on Done button")
				
				$tmr_update = -1 ; Unload update timer
				
				ReleaseKeys()
				ClickControl("done", $poe, "updated") ; Click on Done button
				
				WaitWin(HashLookup($poe, "name"))
			Case "login" ; PoE ready to login
				AddLog("Clicking on login button")
				
				ReleaseKeys()
				ClickControl("login", $poe)
			Case "login_error" ; Failed to login, need to re-type password (will also re-type user)
				ReleaseKeys()
				Send("{esc}")
				
				If $failed_login < HashLookup($config_poe, "max_fail_login") Or HashLookup($config_poe, "max_fail_login") = 0 Or HashLookup($config_poe, "on_max_fail_login") = 0 Then
					If HashLookup($config_poe, "user") <> "" And HashLookup($config_poe, "pass") <> "" Then
						AddLog("Login error, re-typing password (and username)")
						
						ClickControl("user_input", $poe) ; Delete and type user
						Send("{end}+{home}{backspace}" & HashLookup($config_poe, "user"))
						
						ClickControl("pass_input", $poe) ; Delete and type pass
						Send("{end}+{home}{backspace}" & HashLookup($config_poe, "pass"))
						
						ClickControl("login", $poe) ; Click on Login button
					Else
						AddLog("Login error, manual relogging (due to settings, see: [poe] section, user and pass)")
						MsgBox(0, "Login", "Auto-login disabled, please login and click on OK")
					EndIf
					
					$failed_login += 1
				Else
					$failed_login = 0
					
					If HashLookup($config_poe, "on_max_fail_login") > 0 Then
						AddLog("Login error " & ($failed_login + 1) & " times, waiting " & HashLookup($config_poe, "on_max_fail_login") & " seconds")
						Sleep(HashLookup($config_poe, "on_max_fail_login") * 1000)
					ElseIf HashLookup($config_poe, "on_max_fail_login") = -1 Then
						AddLog("Login error " & ($failed_login + 1) & " times, closing the program")
						Exit
					ElseIf HashLookup($config_poe, "on_max_fail_login") <= -2 Then
						AddLog("Login error " & ($failed_login + 1) & " times, shutting down computer")
						
						If MsgBox(262148, "Shutdown", "Failed to login " & $failed_login & " times, do you want to shutdown?" & @CRLF & "Click yes to shutdown now or no to close this program." & @CRLF & "(Note: Auto shutdown after 30 seconds)", 30) = 7 Then Exit
						
						Shutdown(5) ; Force shutdown the computer
					EndIf
				EndIf
			Case "mail" ; E-mail vertification needed
				$code = ""
				
				If HashLookup($config_mail, "user") <> "" And HashLookup($config_mail, "pass") <> "" Then
					For $i = 1 To 2 ; After first fail trying to click the resend button
						AddLog("E-mail vertification needed, reading mails, try: " & $i)
						
						$code = GetFromMail() ; Getting the code
						
						If $code = "" Then
							AddLog("Clicking on re-send button")
							ClickControl("resend", $poe) ; Re-send the e-mail
						Else
							AddLog("Got the code from e-mail: " & $code)
							ExitLoop ; Got the code
						EndIf
					Next
				EndIf
				
				If $code = "" Then ; Failed to get the code or it's disabled
					AddLog("E-mail vertification failed, restarting everything")
					Close()
				EndIf
				
				If $code <> "" And $code <> $last_email_vertif Then ; Writing in the code
					ReleaseKeys()
					
					ClickControl("unlock_input", $poe)
					Send("{end}+{home}{backspace}" & $code)
					
					ClickControl("unlock", $poe)
					
					$last_email_vertif = $code
				Else
					AddLog("E-mail vertification failed, program got the same code as before: " & $code)
				EndIf
			Case "unlocked" ; Successfuly unlocked with e-mail
				ReleaseKeys()
				
				Send("{esc}")
		EndSwitch
	WEnd
#endregion Loop