#include-once

Global $WM_FEEDBACK		= 0xB770	; Feedback from the bot
Global $WM_PING			= 0xB771	; Bot will answer with $WM_FEEDBACK to this
Global $WM_STARTBOT		= 0xB772	; Bot will start and answer with $WM_FEEDBACK
Global $WM_STOPBOT		= 0xB773	; Bot will stop and answer with $WM_FEEDBACK
Global $WM_PAUSE		= 0xB774	; Bot will pause and answer with $WM_FEEDBACK
Global $WM_UNPAUSE		= 0xB775	; Bot will unpause and answer with $WM_FEEDBACK
Global $WM_STATUS		= 0xB776	; Bot will answer with the current status (see: below)
Global $WM_RUNNING		= 0xB777	; Bot will answer this if it is running (see: $WM_STATUS)
Global $WM_PAUSED		= 0xB778	; Bot will answer this if it is paused (see: $WM_STATUS)
Global $WM_STOPPED		= 0xB779	; Bot will answer this if it is stopped (see: $WM_STATUS)
Global $LAST_WM			= 0			; Last window message we got from the bot

; Window will be hidden, need only for communication with Exiled Bot
Global $frmMain = GUICreate("Auto PoE")

; Register each custom window message
For $WM = $WM_FEEDBACK to $WM_STOPPED
	DllCall("user32.dll", "BOOL", "ChangeWindowMessageFilter", "uint", $WM, "DWORD", 1) ; Allow program to get WM-s from outside
	GUIRegisterMsg($WM, "OnWndProc") ; Register window message
Next

; Will be called when receive a $WM_FEEDBACK window message
Func OnWndProc($hHwnd, $iMsg, $wParam, $lParam)
	$LAST_WM = $iMsg
EndFunc

; Posts a message to a specific HWND
Func PostWM($hwnd, $WM)
	$ret = DllCall("user32.dll", "BOOL", "PostMessage", "hwnd", $hwnd, "uint", $WM, "wparam", 0, "lparam", 0)
	
	If Not @error And IsArray($ret) Then
		If $ret[0] = 1 Then
			Return True
		EndIf
	EndIf
	
	Return False
EndFunc

; Waits for a specific window message or until timeout
Func WaitWM($WM = 0, $timeout = 10000)
	Local $tmr = TimerInit()
	Local $ret
	
	Do
		If ($LAST_WM > 0 And $LAST_WM = $WM) Or ($LAST_WM > 0 And $WM = 0) Then
			$tmr = -1
			$ret = $LAST_WM
			$LAST_WM = 0
			Return $ret
		EndIf
	Until TimerDiff($tmr) >= $timeout
	
	$LAST_WM = 0
	Return 0
EndFunc