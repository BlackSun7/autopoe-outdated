#include-once
#include "hash.au3"
#include "NomadMemory.au3"
#include "ImageSearch.au3"
#include "autopoe_config.au3"
#include "autopoe_other.au3"

; Position variables
Global $last_click[2]				; For click delay

; Timers
Global $tmr_click					; For click delay

; do NOT modify $mem here, it is only for read
Global $mem

; Wait until WinExists or TimedOut time
Func WaitWin($names, $timeout = 60000)
	TrayStatus("waiting_for_winexists")
	AddDebug("Waiting for one of the following windows: " & StringReplace($names, ";", ", "))
	
	Local $tmr_waitwin = TimerInit() ; Initialize WaitWin's timer
	Local $parts = StringSplit($names, ";")
	
	While 1
		If TimerDiff($tmr_waitwin) > $timeout  Then ; Wait a maximum of 1 min for win activation
			$tmr_waitwin = -1
			Return ""
		EndIf
		
		For $i = 1 To $parts[0]
			If _WinExists($parts[$i]) Then
				$tmr_waitwin = -1
				Return $parts[$i]
			EndIf
		Next
		
		Sleep(100)
	WEnd
EndFunc

; Wait until WinActive or TimedOut time
Func WaitWinActive($names, $timeout = 60000)
	TrayStatus("waiting_for_winactive")
	AddDebug("Waiting for one of the following windows to be active: " & StringReplace($names, ";", ", "))
	
	Local $tmr_waitwin = TimerInit() ; Initialize WaitWin's timer
	Local $parts = StringSplit($names, ";")
	
	While 1
		If TimerDiff($tmr_waitwin) > $timeout  Then ; Wait a maximum of 1 min for win activation
			$tmr_waitwin = -1
			Return ""
		EndIf
		
		For $i = 1 To $parts[0]
			If Compare(WinGetTitle(""), $parts[$i]) Then
				$tmr_waitwin = -1
				Return $parts[$i]
			EndIf
		Next
		
		Sleep(100)
	WEnd
EndFunc

; Check if window exists or not (regex)
Func _WinExists($title)
	Local $list = WinList()
	
	For $i = 1 To $list[0][0]
		If Compare($list[$i][0], $title) Then
			Return True
		EndIf
	Next
	
	Return False
EndFunc

; Returns the full title from the given title (regex)
Func _WinGetTitle($title)
	Local $list = WinList()
	
	For $i = 1 To $list[0][0]
		If Compare($list[$i][0], $title) Then
			Return $list[$i][0]
		EndIf
	Next
	
	Return ""
EndFunc

; Returns all the windows opened by the process
Func ProcessGetWins($process)
	If IsString($process) Then
		$process = ProcessExists($process)
	EndIf
	
	If $process <= 0 Then Return SetError(1, 0, "")
	
	Local $list = WinList()
	Local $return = ""
	
	For $i = 1 To $list[0][0]
		If $list[$i][0] <> "" And BitAND(WinGetState($list[$i][1]), 2) Then
			If WinGetProcess($list[$i][1]) = $process Then
				$return &= $list[$i][0] & ";"
			EndIf
		EndIf
	Next
	
	If StringRight($return, 1) = ";" Then
		$return = StringTrimRight($return, 1)
	EndIf
	
	Return $return
EndFunc

; Retruns the window name of the given process
Func ProcessGetWin($process)
	If IsString($process) Then $process = ProcessExists($process)
	If $process > 0 Then
		$list = WinList()
		
		For $i = 1 To $list[0][0]
			
			If $list[$i][0] <> "" And BitAND(WinGetState($list[$i][1]), 2) Then
				If WinGetProcess($list[$i][1]) = $process Then
					Return $list[$i][0]
				EndIf
			EndIf
		Next
	EndIf
	
	Return ""
EndFunc

; Click the specified control in specified window, it must have an _x and _y variable in $controls
Func ClickControl($control, ByRef $window, $key = "name")
	If Not WinActive(HashLookup($window, $key)) Then
		WinActivate(HashLookup($window, $key))
		
		If WaitWinActive(HashLookup($window, $key), 5000) = "" Then Return
	EndIf
	
	If WinActive(HashLookup($window, $key)) Then
		$x = HashLookup($controls, $control & "_x")
		$y = HashLookup($controls, $control & "_y")
		
		If ($last_click[0] <> $x Or $last_click[1] <> $y) Or TimerDiff($tmr_click) >= HashLookup($config_settings, "click_delay") * 1000 Then
			MouseClick("left", $x, $y, 1, 0)
			
			$last_click[0] = $x
			$last_click[1] = $y
			$tmr_click = TimerInit()
		EndIf
	EndIf
EndFunc

; Image Search by control
Func Search($control, ByRef $window, $key = "name")
	If Not WinActive(HashLookup($window, $key)) Then
		WinActivate(HashLookup($window, $key))
		
		If WaitWinActive(HashLookup($window, $key), 5000) == "" Then Return 0
	EndIf
	
	If WinActive(HashLookup($window, $key)) Then
		Local $win = WinGetPos(HashLookup($window, $key))
		Local $cs = WinGetClientSize(HashLookup($window, $key))
		
		If Not IsArray($win) Or Not IsArray($cs) Then Return 0
		
		Local $x = $win[0] + (($win[2] - $cs[0]) / 2)
		Local $y = $win[1] + (($win[3] - $cs[1]) - (($win[2] - $cs[0]) / 2))
		
		Local $rx, $ry
		Local $ret =  _ImageSearchArea(		HashLookup($controls, $control & "_bmp"), 1, _
											HashLookup($controls, $control & "_left") + $x, _
											HashLookup($controls, $control & "_top") + $y, _
											HashLookup($controls, $control & "_right") + $x, _
											HashLookup($controls, $control & "_bottom") + $y, $rx, $ry, 80)

		If $ret = 0 And HashLookup($controls, $control & "_bmp_hover") <> "" Then
			$ret =   _ImageSearchArea(		HashLookup($controls, $control & "_bmp_hover"), 1, _
											HashLookup($controls, $control & "_left") + $x, _
											HashLookup($controls, $control & "_top") + $y, _
											HashLookup($controls, $control & "_right") + $x, _
											HashLookup($controls, $control & "_bottom") + $y, $rx, $ry, 80)
		EndIf
		
		Return $ret
	EndIf
	
	Return 0
EndFunc

; Close PoE and the bot
Func Close($type = "")
	If ($type = "" Or Compare($type, ".*error.*")) Then
		Local $filter[2] = [".*[eE]rror.*", ".*[aA]pplication.[eE]rror.*"]
		
		For $i = 0 To UBound($filter) - 1
			While _WinExists($filter[$i])
				WinClose($filter[$i])
			WEnd
		Next
	EndIf
	
	If ($type = "" Or Compare($type, ".*poe.*")) And ProcessExists(HashLookup($poe, "exe")) Then
		If MsgBox(262148, "Closing PoE", "Are you sure you want to close PoE?" & @CRLF & "(NOTE: Auto yes after 5 seconds.)", 5) = 7 Then Exit
		
		_MemoryClose($mem)
		$mem = 0
		$base_address = 0
		
		While ProcessExists(HashLookup($poe, "exe"))
			ProcessClose(HashLookup($poe, "exe"))
		WEnd
	EndIf
	
	If $type = "" Or Compare($type, ".*script.*") Then
		Local $firstAHK = True
		For $i = 0 To HashSize($scripts) - 1
			Local $file = HashKey($scripts, $i)
			
			Local $ext = StringSplit($file, ".")
			$ext = $ext[$ext[0]]
			
			Switch StringLower($ext)
				Case "ahk"
					If $firstAHK Then
						While ProcessExists("AutoHotKey.exe")
							ProcessClose("AutoHotKey.exe")
						WEnd
						$firstAHK = False
					EndIf
				Case "exe"
					Local $prc = StringSplit($file, "\")
					$prc = $prc[$prc[0]]
					
					While ProcessExists($prc)
						ProcessClose($prc)
					WEnd
			EndSwitch
		Next
	EndIf

	If $type = "" Or Compare($type, ".*bot.*") Then
		While ProcessExists(HashLookup($bot, "exe"))
			ProcessClose(HashLookup($bot, "exe"))
		WEnd
		
		While _WinExists(HashLookup($bot, "name"))
			WinClose(_WinGetTitle(HashLookup($bot, "name")))
		WEnd
	EndIf
EndFunc