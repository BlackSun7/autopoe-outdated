#include-once
#include <date.au3>
#include "hash.au3"
#include "pop3.au3"
#include "autopoe_config.au3"
#include "autopoe_other.au3"

; Increase string size with specific character (e.g.: 7 to 07)
Func SetLength($txt, $len, $char = "0", $addbefore = True)
	Local $buf = ""
	
	While StringLen($txt) + StringLen($buf) < $len
		$buf &= $char
	WEnd
	
	If $addbefore Then
		Return StringFormat("%s%s", $buf, $txt)
	Else
		Return StringFormat("%s%s", $txt, $buf)
	EndIf
EndFunc

; Get code from e-mail, (NOTE: SSL is not supported)
Func GetFromMail()
	If _POP3Connect(HashLookup($config_mail, "user"), HashLookup($config_mail, "pass"), HashLookup($config_mail, "pop3"), HashLookup($config_mail, "port")) Then
		For $j = 1 To 5 ; Try 5 times
			Local $mails = _POP3Info() ; Get mails
			
			If Not @error And IsArray($mails) Then ; We have mails in the box, parsing them
				For $i = 1 To $mails[0][0]
					If $mails[$i][3] = "Path of Exile Account Unlock Code" Then
						Local $parts = StringSplit($mails[$i][0], " ")
						
						; Parsing e-mail time
						Local $date = $parts[4] & "/"
						
						Switch $parts[3] ; Parsing mounth names
							Case "Jan"
								$date &= "01"
							Case "Feb"
								$date &= "02"
							Case "Mar"
								$date &= "03"
							Case "Apr"
								$date &= "04"
							Case "May"
								$date &= "05"
							Case "Jun"
								$date &= "06"
							Case "Jul"
								$date &= "07"
							Case "Aug"
								$date &= "08"
							Case "Sep"
								$date &= "09"
							Case "Oct"
								$date &= "10"
							Case "Nov"
								$date &= "11"
							Case "Dec"
								$date &= "12"
						EndSwitch
							
						$date &= "/" & SetLength($parts[2], 2) & " " & $parts[5] ; Adding day and time
						
						Local $timezone = _Date_Time_GetTimeZoneInformation() ; Get current timezone, it's required because PoE will send e-mail in UTC time
						
						If @error Or Not IsArray($timezone) Then
							AddLog("Failed to get system timezone")
							Exit
						EndIf
						
						Local $cur = _NowCalc()
						
						$cur = _DateAdd("n", $timezone[1], $cur)
						
						If $timezone[0] == 2 Then
							$cur = _DateAdd("h", -1, $cur)
						EndIf
						
						If _DateDiff("n", $date, $cur) < 10 Then ; We got the e-mail, parse it
							$code = ""
							
							$msg = StringReplace(StringReplace(_POP3Retr($i), @CRLF, ""), "=0A", "") ; Removing lines (easier to parse)
							
							$tofind = "To play again, you'll need to type or paste the following access code into the game client after logging in:" ; Text to find
							
							$msg = StringMid($msg, StringInStr($msg, $tofind, false) + StringLen($tofind)) ; Search for previous text
							
							$tofind = "Our" ; Find end of the code
							
							Return StringMid($msg, 1, StringInStr($msg, $tofind, false) - 1) ; Return the code
						EndIf
					EndIf
				Next
			EndIf
			
			Local $etmr = TimerInit()
			Do
				_POP3Noop() ; Send a noop every sec to keep connected to the server
				Sleep(1000)
			Until TimerDiff($etmr) >= HashLookup($config_mail, "wait_time") * 1000 ; Wait X sec before retrying
			$etmr = -1
		Next
	EndIf

	_POP3Quit()
	_POP3Disconnect()
	
	Return ""
EndFunc