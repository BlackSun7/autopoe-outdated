#include-once
#include "hash.au3"

Global $ConfigFile = StringTrimRight(@ScriptFullPath, 4) & "_" & @UserName & ".ini" ; Setup config file path

If Not FileExists($ConfigFile) Then ; Config file does not exists, setting it up.
	FileWrite($ConfigFile,	 ";  poe section:" & @CRLF _
							&";    user               -> Your account name (e-mail)" & @CRLF _
							&";    pass               -> Your account password" & @CRLF _
							&';    folderpath         -> The directory where the "PathOfExile.exe" can be found' & @CRLF _
							&";    arguments          -> Command line arguments for PathOfExile.exe (Read more: http://www.pathofexile.com/forum/view-thread/321900)" & @CRLF _
							&";    timeout            -> Restart if idle for this amount of time in minutes (If not logged in, set higher then 10 with slow computer)" & @CRLF _
							&";    shutdown_on_update -> Shutdown computer on PoE update (for safety reason)  (0 = Disabled, 1 = Enabled)" & @CRLF _
							&";    max_update_time    -> Restart if update is not finished after this amount of time in minutes. (0 means disabled)" & @CRLF _
							&";    max_fail_login     -> How many times try to login (0 means disabled)" & @CRLF _
							&";    on_max_fail_login  -> 0: Disabled, >=1: Wait X seconds, -1: Close program, <=-2: Shutdown computer" & @CRLF _
							&";                          Example use:" & @CRLF _
							&";                            This will close the program if failed to login 3 times in a row:" & @CRLF _
							&";                              max_fail_login=3" & @CRLF _
							&";                              on_max_fail_login=-1" & @CRLF _
							&";                            " & @CRLF _
							&";                            This will shutdown the computer after 5 login error in a row:" & @CRLF _
							&";                              max_fail_login=5" & @CRLF _
							&";                              on_max_fail_login=-2" & @CRLF _
							&";                            " & @CRLF _
							&";                            This will wait 10 seconds before re-trying to login (after 2 login error):" & @CRLF _
							&";                              max_fail_login=2" & @CRLF _
							&";                              on_max_fail_login=10" & @CRLF _
							&";" & @CRLF _
							&";  bot section:" & @CRLF _
							&';    folderpath         -> The directory where the "ExiledBot.exe" can be found' & @CRLF _
							&";    timeout            -> Restart if idle for this amount of time in minutes. (If logged in running with bot)" & @CRLF _
							&";" & @CRLF _
							&";  mail section:" & @CRLF _
							&";    user               -> Your e-mail address (Empty means disabled)" & @CRLF _
							&";    pass               -> Your e-mail password (Empty means disabled)" & @CRLF _
							&";    pop3               -> Your e-mail host's pop3 server (SSL does not working, mail.aol.com is recommended)" & @CRLF _
							&";    port               -> Your e-mail host's server port (usually 110)" & @CRLF _
							&";    wait_time          -> Wait this amount of time in seconds before retrying e-mail reading" & @CRLF _
							&";" & @CRLF _
							&";  settings section" & @CRLF _
							&";    logging            -> Enable logging (0: Disabled, 1: Enabled)" & @CRLF _
							&";    debugging          -> Enable debug logging (a lots of log) (0: Disabled, 1: Enabled)" & @CRLF _
							&";    log_file           -> Log file (if enabled)" & @CRLF _
							&";    click_delay        -> Program will not click again for this amount of time in seconds" & @CRLF _
							&";" & @CRLF _
							&";  scripts section" & @CRLF _
							&";    runfolder_         -> Will run every file with the extension specified down (NOTE: write key after _, eg a number)" & @CRLF _
							&";    runext_            -> Will set the extensions (NOTE: write key after _, this will link the two settings together)" & @CRLF _
							&";    runscript          -> If you don't want to run every script in a folder you can use this, it will run only one file" & @CRLF _
							&";" & @CRLF _
							&";       Example usage:" & @CRLF _
							&";          runfolder_1=C:\Scripts" & @CRLF _
							&";          runext_1=exe|ahk" & @CRLF _
							&";          ^ This two line will run every ahk and exe file found in C:\Scripts ^" & @CRLF _
							&";" & @CRLF _
							&";          runfolder_2=C:\Scripts_2" & @CRLF _
							&";          runext_2=exe" & @CRLF _
							&";          ^ This two line will run every exe file found in C:\Scripts_2 ^          " & @CRLF _
							&";" & @CRLF _
							&";          runscript=C:\OneScriptOnly\test.ahk" & @CRLF _
							&";          ^ This line will runy only test.ahk found in C:\OneScriptOnly ^" & @CRLF _
							&";" & @CRLF _
							&";    NOTE: It will not detect if script is running or not, also it will close process (if exe extension) before run," & @CRLF _
							&";          and AutoHotKey.exe before running autohotkey scripts" & @CRLF _
							&@CRLF&@CRLF _
							&"[poe]" _
							&@CRLF _
							&"user=" _
							&@CRLF _
							&"pass=" _
							&@CRLF _
							&"folderpath=" & @ScriptDir _
							&@CRLF _
							&"arguments=" _
							&@CRLF _
							&"timeout=20" _
							&@CRLF _
							&"shutdown_on_update=1" _
							&@CRLF _
							&"max_update_time=0" _
							&@CRLF _
							&"max_fail_login=0" _
							&@CRLF _
							&"on_max_fail_login=0" _
							&@CRLF&@CRLF _
							&"[bot]" _
							&@CRLF _
							&"folderpath=" & @ScriptDir _
							&@CRLF _
							&"timeout=5" _
							&@CRLF&@CRLF _
							&"[mail]" _
							&@CRLF _
							&"user=" _
							&@CRLF _
							&"pass=" _
							&@CRLF _
							&"pop3=" _
							&@CRLF _
							&"port=110" _
							&@CRLF _
							&"wait_time=30" _
							&@CRLF&@CRLF _
							&"[settings]" _
							&@CRLF _
							&"logging=1" _
							&@CRLF _
							&"debugging=0" _
							&@CRLF _
							&"panic_key=F10" _
							&@CRLF _
							&"pause_key=F9" _
							&@CRLF _
							&"log_file=" & StringTrimRight(@ScriptFullPath, 4) & "_" & @UserName & ".log" _
							&@CRLF _
							&"click_delay=2" _
							&@CRLF&@CRLF _
							&"[scripts]")
							
	If MsgBox(262148, "Config", "Config file generated, do you want to open it now?" & @CRLF & "Click yes to open the config files and to close this program or no to continue") = 6 Then
		ShellExecute($ConfigFile)
		Exit
	EndIf
EndIf

; Load settings to RAM for less IO
Global $config_poe =		HashReadIniSection($ConfigFile, "poe")
Global $config_bot =		HashReadIniSection($ConfigFile, "bot")
Global $config_mail =		HashReadIniSection($ConfigFile, "mail")
Global $config_settings =	HashReadIniSection($ConfigFile, "settings")

Global $poe =				HashCreateEmpty()
							HashInsert($poe, "name", "Path of Exile")
							HashInsert($poe, "updating", ".*%  Speed: .*/sec  .* of .*")
							HashInsert($poe, "checking_resources", "Checking resources...")
							HashInsert($poe, "allocating_space", "Allocating Space...")
							HashInsert($poe, "updated", "Done")
							HashInsert($poe, "error", "Connection Failure")
							HashInsert($poe, "error2", ".*[eE]rror.*")
							HashInsert($poe, "exe", "PathOfExile.exe")
							HashInsert($poe, "handle", 0)
							HashInsert($poe, "pid", 0)
						
Global $bot = 				HashCreateEmpty()
							HashInsert($bot, "name", ".*Exiled.*Bot.*")
							HashInsert($bot, "notify", ".*Open.*beta.*released.*")
							HashInsert($bot, "exename", ".*ExiledBot.*[.]exe")
							HashInsert($bot, "exe", FindBotExe())
							HashInsert($bot, "error", ".*[eE]rror.*")
							HashInsert($bot, "handle", 0)

Global $controls = 			HashCreateEmpty()

							; Buttons with Bitmaps
							HashInsert($controls, "login_left", 521)
							HashInsert($controls, "login_top", 378)
							HashInsert($controls, "login_right", 603)
							HashInsert($controls, "login_bottom", 408)
							HashInsert($controls, "login_x", 563)
							HashInsert($controls, "login_y", 394)
							HashInsert($controls, "login_bmp", @ScriptDir & "\Resources\login.bmp")
							HashInsert($controls, "login_bmp_hover", @ScriptDir & "\Resources\login_hover.bmp")
							
							HashInsert($controls, "unlock_left", 520)
							HashInsert($controls, "unlock_top", 331)
							HashInsert($controls, "unlock_right", 600)
							HashInsert($controls, "unlock_bottom", 359)
							HashInsert($controls, "unlock_x", 562)
							HashInsert($controls, "unlock_y", 344)
							HashInsert($controls, "unlock_bmp", @ScriptDir & "\Resources\unlock.bmp")
							HashInsert($controls, "unlock_bmp_hover", @ScriptDir & "\Resources\unlock_hover.bmp")
							
							HashInsert($controls, "loginerror_left", 369)
							HashInsert($controls, "loginerror_top", 266)
							HashInsert($controls, "loginerror_right", 431)
							HashInsert($controls, "loginerror_bottom", 281)
							HashInsert($controls, "loginerror_bmp", @ScriptDir & "\Resources\login_error.bmp")
							
							HashInsert($controls, "abnormaldisconnection_left", 339)
							HashInsert($controls, "abnormaldisconnection_top", 236)
							HashInsert($controls, "abnormaldisconnection_right", 461)
							HashInsert($controls, "abnormaldisconnection_bottom", 311)
							HashInsert($controls, "abnormaldisconnection_bmp", @ScriptDir & "\Resources\abnormal_disconnection.bmp")
							
							HashInsert($controls, "unlocked_left", 351)
							HashInsert($controls, "unlocked_top", 261)
							HashInsert($controls, "unlocked_right", 446)
							HashInsert($controls, "unlocked_bottom", 274)
							HashInsert($controls, "unlocked_bmp", @ScriptDir & "\Resources\account_unlocked.bmp")
							
							; Click only buttons or inputs
							HashInsert($controls, "done_x", 642)
							HashInsert($controls, "done_y", 323)
							
							HashInsert($controls, "unlock_input_x", 396)
							HashInsert($controls, "unlock_input_y", 256)
							
							HashInsert($controls, "resend_x", 397)
							HashInsert($controls, "resend_y", 343)
							
							HashInsert($controls, "user_input_x", 490)
							HashInsert($controls, "user_input_y", 390)
							
							HashInsert($controls, "pass_input_x", 490)
							HashInsert($controls, "pass_input_y", 415)

; Contains the scripts
Global $scripts = HashCreateEmpty()

; Convert IniReadSection to Hash (hash.au3)
Func HashReadIniSection($file, $section)
	If FileExists($file) Then
		$read = IniReadSection($file, $section)
		
		If IsArray($read) Then
			$hash = HashCreateEmpty()
			
			For $i = 1 To $read[0][0]
				HashInsert($hash, $read[$i][0], $read[$i][1])
			Next
			
			Return $hash
		Else
			Return HashCreateEmpty()
		EndIf
	EndIf
EndFunc

; Returns the exe name of the bot
Func FindBotExe()
	Local $search = FileFindFirstFile(HashLookup($config_bot, "folderpath") & "\*.*")
	If $search = -1 Then Return SetError(1, 0, 0)
	
	Local $file = ""
	Do
		$file = FileFindNextFile($search)
	Until @error Or StringRegExp($file, HashLookup($bot, "exename"))
	
	If $file <> "" Then
		Return $file
	Else
		Return 0
	EndIf
EndFunc

; Checks if resource file is okay for this control or not
Func CheckResource(ByRef $hash, $control)
	If HashLookup($hash, $control & "_bmp") <> "" Then
		If Not FileExists(HashLookup($hash, $control & "_bmp")) Then
			Return False
		EndIf
	EndIf
	
	If HashLookup($hash, $control & "_bmp_hover") <> "" Then
		If Not FileExists(HashLookup($hash, $control & "_bmp_hover")) Then
			Return False
		EndIf
	EndIf
	
	Return True
EndFunc

; Get script files from the settings
Func GetScripts()
	Local $config_scripts = HashReadIniSection($ConfigFile, "scripts")
	Local $size = HashSize($config_scripts)
	
	If $size > 0 Then
		Local $runs = HashCreateEmpty()
		Local $exts = HashCreateEmpty()
		
		For $i = 0 To $size - 1
			Local $key = HashKey($config_scripts, $i)
			Local $value = HashData($config_scripts, $i)
			
			If StringRegExp($key, "runfolder_.*") Then
				Local $parts = StringSplit($key, "_")
				
				If $parts[0] = 2 Then
					Local $dir = $value
					
					If StringRight($dir, 1) = "\" Then
						$dir = StringTrimRight($dir, 1)
					EndIf
					
					Local $search = FileFindFirstFile($dir & "\*.*")
					
					Do
						Local $file = FileFindNextFile($search)
						
						If $file <> "" Then
							HashInsert($runs, $dir & "\" & $file, $parts[2])
						EndIf
					Until $file = ""
				EndIf
			ElseIf StringRegExp($key, "runext_.*") Then
				Local $parts = StringSplit($key, "_")
				
				If $parts[0] = 2 Then
					If StringRegExp($value, ".*|.*") Then
						Local $split = StringSplit($value, "|")
						
						For $j = 1 To $split[0]
							HashInsert($exts, $split[$j], $parts[2])
						Next
					Else
						HashInsert($exts, $value, $parts[2])
					EndIf
				EndIf
			ElseIf StringRegExp($key, "runscript") Then
				HashInsert($runs, $value, "MustRun")
			EndIf
		Next
		
		Local $i = 0
		While $i <= HashSize($runs) - 1
			Local $key = HashData($runs, $i)
			Local $file = HashKey($runs, $i)
			
			If StringRegExp($key, "MustRun") Then
				$i += 1
				ContinueLoop
			EndIf
			
			Local $remove = True
			For $j = 0 To HashSize($exts) - 1
				Local $ext = StringSplit($file, ".")
				$ext = $ext[$ext[0]]
				
				If StringRegExp(StringLower(HashKey($exts, $j)), StringLower($ext)) Then
					$remove = False
					ExitLoop
				EndIf
			Next
			
			If $remove Then
				HashRemove($runs, $file)
				$i = 0
			Else
				$i += 1
			EndIf
		WEnd
		
		$scripts = $runs
	Else
		$scripts = HashCreateEmpty()
	EndIf
EndFunc