#include-once
#include "hash.au3"
#include "autopoe_config.au3"
#include "autopoe_other.au3"
#include "autopoe_winhandler.au3"

Global $paused = False ; Flag if script is paused

; Panic (To close the program instantly)
Local $key
If HashLookup($config_settings, "panic_key") = "" Then
	$key = "{F10}"
Else
	$key = "{" & HashLookup($config_settings, "panic_key") & "}"
EndIf

HotKeySet($key, "PANIC")
Func PANIC()
	AddLog("Panic button pressed")
	Close("script")
	Exit
EndFunc

; Pause
If HashLookup($config_settings, "pause_key") = "" Then
	$key = "{F9}"
Else
	$key = "{" & HashLookup($config_settings, "pause_key") & "}"
EndIf

HotKeySet($key, "Toggle")
Func Toggle()
	$paused = Not $paused
	If $paused Then
		TrayTip("Pause", "Script is paused", 1)
		TraySetToolTip("Auto PoE Restarter" & @CRLF & "Current status: paused")
	Else
		TrayTip("Pause", "Script running", 1)
	EndIf
EndFunc

;Reload settings
If HashLookup($config_settings, "reload__key") = "" Then
	$key = "{F8}"
Else
	$key = "{" & HashLookup($config_settings, "pause_key") & "}"
EndIf

HotKeySet($key, "Reload")
Func Reload()
	$config_poe = HashReadIniSection($ConfigFile, "poe")
	$config_bot = HashReadIniSection($ConfigFile, "bot")
	$config_mail = HashReadIniSection($ConfigFile, "mail")
	$config_settings = HashReadIniSection($ConfigFile, "settings")
	
	GetScripts()
	
	HashUpdate($bot, "exe", FindBotExe())
EndFunc