#include-once
#include <date.au3>
#include "hash.au3"
#include "autopoe_config.au3"

Global $last_log = "" ; To prevent same log text spamming

; Adds the specific text with date and time to the log file
Func AddLog($txt) 
	If HashLookup($config_settings, "logging") > 0 And (HashLookup($config_settings, "debugging") > 0 Or $txt <> $last_log) Then
		If HashLookup($config_settings, "debugging") > 0 Then
			FileWriteLine(HashLookup($config_settings, "log_file"), "[Normal] [" & _Now() & "]: " & $txt)
		Else
			FileWriteLine(HashLookup($config_settings, "log_file"), "[" & _Now() & "]: " & $txt)
		EndIf
		
		$last_log = $txt
	EndIf
EndFunc

; Adds the specific text with date and time to the debug file
Func AddDebug($txt)
	If HashLookup($config_settings, "debugging") > 0 Then
		FileWriteLine(HashLookup($config_settings, "log_file"), "[Debug]  [" & _Now() & "]: " & $txt)
	EndIf
EndFunc

; Run every script
Func RunScripts()
	For $i = 0 To HashSize($scripts) - 1
		Local $file = HashKey($scripts, $i)
		
		If Not FileExists($file) Then ContinueLoop
		
		Local $parts = StringSplit($file, "\")
		Local $workdir = ""
		For $j = 1 To $parts[0] - 1
			$workdir &= $parts[$j] & "\"
		Next
		
		ShellExecute($file, "", $workdir)
	Next
EndFunc

; Compare two string (regex)
Func Compare($str1, $str2)
	If StringRegExp($str1, $str2) Then Return True
	Return False
EndFunc

; Set Tray tool tip
Func TrayStatus($status = "", $bot_status = "")
	Local $msg = "Auto PoE Restarter"
	
	If $status <> "" Then
		$msg &= @CRLF
		$msg &= "Current status: "
		$msg &= $status
	EndIf
	
	If $bot_status <> "" Then
		$msg &= @CRLF
		$msg &= "Current bot status: "
		$msg &= $bot_status
	EndIf
	
	TraySetToolTip($msg)
EndFunc